# Installation
1. Clone the repository
2. In Terminal, navigate to the root directory of the project and run `npm install`.

# Build & Run
1. Run `npm run serve`
2. In your browser, navigate to https://localhost:9000