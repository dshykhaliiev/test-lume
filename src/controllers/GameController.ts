import {Inject} from "engine-api/system/serviceContainer/InjectDecorator";
import {GameModel} from "../models/GameModel";
import {ServicesEnum} from "../consts/ServicesEnum";
import {IContainer} from "engine-api/system/componentModel/IContainer";
import {Application} from "engine-api/system/application/Application";
import {GameView} from '../views/GameView';
import {Ticker} from "engine-api/time/Ticker";

export class GameController {
  @Inject(ServicesEnum.GAME_MODEL)
  private _gameModel: GameModel;

  private _gameView: GameView;

  constructor() {
    this._init();
  }

  private _init(): void {
    this._initViews();


    this._addListeners();
  }

  private _addListeners(): void {
  }

  private _onBuyCard(): void {
    this._startGame();
  }

  private _startGame(): void {

  }

  private _onGameFinished(): void {
  }

  private _initViews(): void {
    const game: IContainer = Application.game.getChild('game');

    this._gameView = game.getChild('gameView');


    const ticker = new Ticker();
    ticker.onTickEvent.addListener(() => {
      this._gameView.update();
    });
    ticker.start();
  }
}