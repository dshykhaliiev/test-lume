import { IManifestData } from 'engine/framework/resource/manifest/IManifestData';
import {AtlasType, TextureFormat} from "engine-api/resource/types/Formats";

export function GetSpineManifest(): IManifestData {
  return {
    data: {
      id: 'spineManifest',
      files: {


        // Spine Boy
        'spineboy.png': {
          src: 'spineboy.png',
          type: 'image/png',
          embedded: false
        },
        'spineboy.atlas': {
          src: 'spineboy.atlas',
          type: 'application/text',
          embedded: false
        },
        'spineboy.json': {
          src: 'spineboy.json',
          type: 'application/json',
          embedded: false
        },
      },
      // @ts-ignore
      resources: {
        skeletalLayersDemo: {
          spineboy: {
            type: 'skeletalAsset',
            files: [
              { name: 'spineboy.png', extension: TextureFormat.PNG },
              { name: 'spineboy.atlas', extension: AtlasType.ATLAS },
              { name: 'spineboy.json', extension: AtlasType.JSON }
            ]
          }
        },
      }
    }
  };
}

export function GetExamplesResourceMap(): Map<string, string> {
  const resourceMap: Map<string, string> = new Map();

  // Spine Boy
  resourceMap.set('spineboy.png', `spine/spineboy.png`);
  resourceMap.set('spineboy.atlas', `spine/spineboy.atlas`);
  resourceMap.set('spineboy.json', `spine/spineboy.json`);

  return resourceMap;
}
