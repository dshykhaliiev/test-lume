export class Button {
    private _enabled = false;

    public setEnabled() {
      this._enabled = true;
    }

    public setDisabled() {
      this._enabled = false;
    }

    public getEnabled() {
      return this._enabled;
    }
}