import {Container} from "engine-api/system/componentModel/Container";
import {IContainer} from "engine-api/system/componentModel/IContainer";
import {ISymbol} from "./ISymbol";
import {Sprite} from "engine-api/controls/Sprite";
import {SymbolConsts} from "../../../consts/SymbolConts";
import {Mask} from "engine-api/controls";
import {Circle} from "engine-api/graphics/shapes/Circle";
import {rgb} from "engine/core/common/Utilities";
import {Pointer} from "engine/framework/input/Pointer";
import {HitEventType} from "engine-api/input/events/HitEventType";
import {HitEvent} from "engine-api/input/events/HitEvent";
import {IPoint} from "engine/core/common/types/IPoint";
import {SpriteFactory} from "./helpers/SpriteFactory";
import {ParticleGroup} from "engine-api/particle/ParticleGroup";
import {GetParticleProps} from "./helpers/ParticleProps";
import {IEvent, IEventListener} from "engine/framework/system/event/Events";
import {Event} from "engine/core/common/event/Event";

const EVENT_STEP: number = 5;
const BRUSH_SIZE: number = 20;

export class Symbol extends Container implements ISymbol {
  private _iconSprite: Sprite;
  private _coverSprite: Sprite;
  private _coverContainer: IContainer;
  private _shapeMask: Mask;
  private _pointer: Pointer;
  private _totalArea: number;
  private _cleanedArea: number;
  private _eventCounter: number;
  private _particleContainer: IContainer;
  private _particleSystem: ParticleGroup;
  private _isRevealed: boolean;

  private _revealedEvent: IEvent = new Event();

  constructor(parent: IContainer, id: string) {
    super(parent, id);
  }

  public setEnabled(value: boolean): void {
    this.enabled = value;
  }

  public setTextureId(id: string): void {
    this._iconSprite.textureId = id;
  }

  protected _init() {
    this._createIcon();
    this._createCover();

    this._pointer = new Pointer(this._iconSprite);
    this._pointer.on(HitEventType.PAN, this._onPointerMove.bind(this));
    this._pointer.on(HitEventType.TAPSTART, (event: HitEvent): void => {
      if (this.enabled) {
        const local: IPoint = this._coverContainer.localXY(event.canvasPosition);
        this._particleContainer.x = local.x - this.width / 2;
        this._particleContainer.y = local.y - this.height / 2;
        this._particleSystem.play();
      }
    });
    this._pointer.on(HitEventType.PANEND, () => {
      this._particleSystem.stop();
    });
    this._pointer.on(HitEventType.TAP, () => {
      this._particleSystem.stop();
    });

    this._createMask();

    this._totalArea = this._coverSprite.width * this._coverSprite.height;

    this.enabled = false;

    this._createParticles();

  }

  public reveal(): void {
    this.setEnabled(false);
    this._coverContainer.visible = false;
    this._particleSystem.stop();
    this._isRevealed = true;

    this._revealedEvent.emit();
  }

  public get revealedEvent(): IEventListener {
    return this._revealedEvent.asEventListener();
  }

  public get isRevealed(): boolean {
    return this._isRevealed;
  }

  public reset(): void {
    this._cleanedArea = 0;
    this._eventCounter = 0;
    this._coverContainer.visible = true;
    this._shapeMask.shapes = [];
    this._isRevealed = false;
  }

  private _createIcon(): void {
    this._iconSprite = this.createComponent(Sprite, `${this.id}_icon`, {
      textureId: 'icon_0',
      width: SymbolConsts.WIDTH,
      height: SymbolConsts.HEIGHT,
    });
  }

  private _createCover(): void {
    this._coverContainer = this.createComponent(Container, `${this.id}_coverContainer`);
    this._coverContainer.z = 2;
    this._coverContainer.propagates = true;

    this._coverSprite = this._coverContainer.createComponent(Sprite, `${this.id}_cover`, {
      textureId: 'icon_back',
      width: SymbolConsts.WIDTH,
      height: SymbolConsts.HEIGHT,
    });
    this._coverSprite.propagates = true;
  }

  private _createMask(): void {
    this._shapeMask = this._coverContainer.createComponent(Mask, `${this.id}_mask`);
    this._shapeMask.z = 2;
    this._shapeMask.x = 0;
    this._shapeMask.y = 0;
    this._shapeMask.inverse = true;
    this._shapeMask.shapes = [];
    this._coverContainer.mask = this._shapeMask;
  }

  private _onPointerMove(event: HitEvent): void {
    if (!this.enabled) {
      return;
    }

    this._eventCounter++;

    const local: IPoint = this._coverContainer.localXY(event.canvasPosition);
    const circle = this._createMaskShape();
    circle.x = local.x - this.width / 2;
    circle.y = local.y - this.height / 2;
    this._shapeMask.shapes = [...this._shapeMask.shapes, circle];

    this._particleContainer.x = local.x;
    this._particleContainer.y = local.y;

    if (local.x >= 100 || local.y <= 20) {
      this._particleSystem.stop();
    }

    if (this._eventCounter % EVENT_STEP !== 0) {
      return;
    }

    this._cleanedArea += Math.PI * circle.radius * circle.radius;
    if (this._cleanedArea >= this._totalArea) {
      this.reveal();
    }
  }

  private _createMaskShape(): Circle {
    return new Circle(
      0,0, BRUSH_SIZE,
      { width: 1, lineColor: rgb(0, 0, 255), lineAlpha: 1 },
    );
  }

  private _createParticles(): void {
    this._particleContainer = this.createComponent(Container, 'particleContainer');
    this._particleContainer.z = 10;
    const factory = new SpriteFactory(this._particleContainer);
    const particlesProps = GetParticleProps(factory);
    this._particleSystem = new ParticleGroup(particlesProps, this._particleContainer);
  }
}
