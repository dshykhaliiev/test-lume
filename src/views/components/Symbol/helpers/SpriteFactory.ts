import {IParticleFactory} from "engine/core/subsystems/particles/IParticleFactory";
import {Sprite} from "engine-api/controls/Sprite";
import {IComponent} from "engine-api/system/componentModel/IComponent";
import {IDGenerator} from "engine/core/common/IDGenerator";
import {ComponentProperties} from "engine/framework/Types";
import {BlendMode} from "engine/core/subsystems/render/BlendMode";

export class SpriteFactory implements IParticleFactory<Sprite> {
  private _owner: IComponent;
  private _count: number;
  private readonly _id: string;
  public constructor(owner: IComponent) {
    this._owner = owner;
    this._count = 0;
    this._id = IDGenerator.generate();
  }
  public create(): Sprite {
    const spriteProps: ComponentProperties<Sprite> = {
      textureId: 'particle',
      blendMode: BlendMode.NORMAL,
      width: 10,
      height: 10,
      originX: 0.5,
      originY: 0.5
    };
    return this._owner.createComponent(Sprite, this._id + this._count++, spriteProps);
  }
  public onParticleCreated(particle: Sprite): void {
    particle.init();
  }
  public onParticleUpdated(particle: Sprite): void {
    //
  }
  public onParticleDead(particle: Sprite): void {
    this._owner.removeAndDisposeComponent(particle);
  }
}