import {Event} from "engine/core/common/event/Event";
import {Container} from "engine/framework/controls";
import {Sprite} from "engine/framework/controls/Sprite";
import {HitEventType} from "engine/framework/input/events/HitEventType";
import {Pointer} from "engine/framework/input/Pointer";
import {IEvent, IEventListener} from "engine/framework/system/event/Events";
import {RegisterClass} from "engine/framework/system/registry/ClassRegistryDecorators";
import {ComponentProperties, ComponentReadonlyProperties} from "engine/framework/Types";
import {Text, TextType} from 'engine/framework/controls/Text';
import {AudioSprite} from "engine-api/sound/AudioSprite";
import {AudioPlayer} from "engine-api/sound/AudioPlayer";


@RegisterClass()
export class SimpleButton extends Container {

  public onTexture: string;
  public hoverTexture: string;
  public downTexture: string;
  private _isHovered: boolean = false;
  private _clickedEmitter: IEvent = new Event();
  private _buttonSprite: Sprite;
  private _defaultTexture: string;
  private _label: string = '';
  private _text: Text;

  public get clickEvent(): IEventListener {
    return this._clickedEmitter.asEventListener();
  }

  public set textureId(value: string) {
    this._defaultTexture = value;
    if (this._buttonSprite == null) {
      return;
    }
    this._buttonSprite.textureId = value;
  }

  protected _create(properties: ComponentReadonlyProperties<this>): void {
    super._create(properties);
    this.onTexture = properties.onTexture;
    this.hoverTexture = properties.hoverTexture;
    this.downTexture = properties.downTexture;
    this.textureId = properties.onTexture;
  }

  public set label(value: string) {
    this._label = value;

    this._text.text = value;
  }

  public updatePosition(x: number, y: number, z: number, width: number, height: number): void {
    super.updatePosition(x, y, z, width, height);
    this._buttonSprite.width = width;
    this._buttonSprite.height = height;
  }

  protected _init(): void {
    super._init();

    this._buttonSprite = this.createComponent(Sprite, 'sprite', {
      width: 200,
      height: 50,
      textureId: this._defaultTexture,
      propagates: true
    });
    const pointer = new Pointer(this);
    pointer.on([HitEventType.MOUSEIN, HitEventType.MOUSEOUT, HitEventType.TAPSTART, HitEventType.TAPCANCEL, HitEventType.TAP], (evt) => {
      if (evt.eventType === HitEventType.MOUSEIN) {
        this._isHovered = true;
        this.textureId = this.hoverTexture;
      } else if (evt.eventType === HitEventType.MOUSEOUT) {
        this._isHovered = false;
        this.textureId = this.onTexture;
      } else if (evt.eventType === HitEventType.TAPSTART) {
        this.textureId = this.downTexture;
      } else if (evt.eventType === HitEventType.TAP || evt.eventType === HitEventType.TAPCANCEL) {
        this.textureId = this._isHovered ? this.hoverTexture : this.onTexture;
        this._clickedEmitter.emit();

        const sfx = new AudioSprite('audio', 'Menu_Click');
        const player = new AudioPlayer();
        player.play(sfx);
      }
    });

    this._createText();

    this._createSound();
  }

  private _createSound(): void {

  }

  private _createText(): void {
    const textProps: ComponentProperties<Text> = {
      text: '',
      textType: TextType.CANVAS,
      fontSize: 24,
      z: 21,
      originX: 0.5,
      originY: 0.5,
    };

    this._text = this.createComponent(Text, `${this.id}_text`, textProps);
    this._text.x = this._buttonSprite.width / 2;
    this._text.y = 22;
  }
}