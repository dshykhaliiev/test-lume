import {Container} from "engine-api/system/componentModel/Container";
import {IContainer} from "engine-api/system/componentModel/IContainer";
import {RegisterClass} from "engine-api/system/registry/ClassRegistryDecorators";
import {ICard} from "./ICard";
import {SimpleButton} from "../Button/SimpleButton";
import {Event} from "engine/core/common/event/Event";
import {IEvent, IEventListener} from "engine/framework/system/event/Events";
import {ISymbol} from "../Symbol/ISymbol";
import {Symbol} from "../Symbol/Symbol";
import {SymbolConsts} from "../../../consts/SymbolConts";

const CARD_SIZE: number = 3;

@RegisterClass()
export class Card extends Container implements ICard {
  private _symbols: ISymbol[] = [];

  private _eventEmitter: IEvent<number> = new Event<number>();
  private _cardCompleteEvent: IEvent = new Event();

  constructor(parent: IContainer, id: string) {
    super(parent, id);
  }


  public startGame(symbols: number[]): void {
    this._symbols.forEach((symbol: ISymbol, index: number): void => {
      symbol.setTextureId(`icon_${symbols[index]}`);
      symbol.setEnabled(true);
      symbol.reset();
    });
  }

  public revealAll(): void {
    this._symbols.forEach((symbol: ISymbol): void => {
      symbol.reveal();
    });
  }

  protected _init() {
    super._init();

    let index: number = 0;
    for (let row = 0; row < CARD_SIZE; row++) {
      for (let col = 0; col < CARD_SIZE; col++) {
        this._symbols.push(this._createSymbol(index, row, col));
        index++;
      }
    }
  }

  private _createSymbol(id: number, row: number, col: number): ISymbol {
    const symbol: ISymbol = this.createComponent(Symbol, `symbol_${id}`);
    symbol.x = col * (SymbolConsts.WIDTH + SymbolConsts.GAP_H);
    symbol.y = row * (SymbolConsts.HEIGHT + SymbolConsts.GAP_V);
    symbol.revealedEvent.addListener(this._onSymbolRevealed, this);
    return symbol;
  }

  private _onSymbolRevealed(): void {
    const revealed: ISymbol[] = this._symbols.filter(symbol => !symbol.isRevealed);
    if (revealed.length === this._symbols.length) {
      this._cardCompleteEvent.emit();
    }
  }

  public get cardCompleteEvent(): IEventListener {
    return this._cardCompleteEvent.asEventListener();
  }

  public get onClick(): IEventListener<number> {
    return this._eventEmitter.asEventListener();
  }
}