import {Container} from "engine-api/system/componentModel/Container";
import {RegisterClass} from "engine-api/system/registry/ClassRegistryDecorators";
import {Drawer} from "engine-api/graphics/Drawer";
import {Rectangle} from "engine-api/graphics/shapes/Rectangle";
import {Application} from "engine-api/system/application/Application";
import {Button} from "./Button";

@RegisterClass()
export class GameView extends Container {
  private _user;
  dims;
  box1;

  constructor(parent, id) {
    super(parent, id);
  }

  _init() {
    super._init();

    this.dims = Application.engine.platform.dimensions.value;

    const rect = new Rectangle(
        0, 0, 100, 100, undefined,
        {
          width: 10,
          lineColor: {
            red: 255,
            green: 255,
            blue: 255
          },
          lineAlpha: 1
        },
        true,
        {
          red: 0,
          green: 255,
          blue: 150
        }
    );

    const user = this.createComponent(Drawer, 'user', {
      shapes: [rect]
    });
    user.init();

    this._user = user;
    // TODO: Init complete, put your code here

    let lineStyle = {
      width: 10,
      lineColor: {
        red: 0,
        green: 0,
        blue: 255
      },
      lineAlpha: 1
    }

    let rectangle = new Rectangle(300, 300,
      300, 50, 1,
      lineStyle, true,
      {
        red: 255,
        green: 255,
        blue: 0
      }, 1);

    let box1 = this.createComponent(Drawer, 'box1', {
      shapes: [rectangle]
    }, true);

    rectangle.height = 200;

    box1.x = 500;

    this.box1 = box1;

    this.box1.x = this.dims.width / 2;// - this.box1.width / 2;
  }

  update() {
    // TODO: called every tick
    // this._user.x += 0.3;
    // console.log('update')
    // this.box1.x -= 0.5;
  }
}
