import {ScratchGame} from "./ScratchGame";
import {IEngineConfig} from "engine/core/EngineConfig";
import {LogLevel} from "engine/core/common/logger/Logger";
import {Application} from "engine-api/system/application/Application";
import {GetExamplesResourceMap} from "./consts/spineAssets";

function initGame(): void {
  const config: IEngineConfig = {
    audioEnabled: true,
    canvasElement: 'canvasView',
    mute: false,
    backgroundColor: 0x000000,
    resourceHashMap: GetExamplesResourceMap(),
    logLevel: LogLevel.DEBUG,
  }

  Application.initialize(config);
  Application.createGame(ScratchGame, 'scratchGame');
  Application.run();
  Application.game.init();
}

initGame();