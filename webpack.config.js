var webpack = require('webpack');
var path = require('path');

module.exports = {
  entry: path.resolve(__dirname, './src/index.ts'),
  output: {
    filename: 'index.js',
    path: path.resolve(__dirname, 'dist'),
    pathinfo: false
  },
  plugins: [
    new webpack.LoaderOptionsPlugin({
      options: {
        worker: {
          output: {
            path: path.resolve(__dirname, './dist'),
          }
        }
      }
    })
  ],
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
    ]
  },
  resolve: {
    modules: [
      'node_modules',
      path.resolve(__dirname, './src')
    ],
    alias: {
      'engine-api': path.resolve(__dirname, './node_modules/@hyperdrive/engine/dist/es6/framework'),
      'engine': path.resolve(__dirname, './node_modules/@hyperdrive/engine/dist/es6/')
    },
    extensions: ['.ts', '.js', '.min.js']
  },
  devServer: {
    contentBase: path.resolve(__dirname, 'resources'),
    compress: true,
    host: '0.0.0.0',
    port: 9001,
    inline: false,
    watchContentBase: false
  },
  devtool: 'source-map',
  cache: true,
  mode: 'development',
  stats: {
    cached: false,
    cachedAssets: false,
    chunks: false,
    chunkModules: false,
    chunkOrigins: false,
    modules: false
  }
};
